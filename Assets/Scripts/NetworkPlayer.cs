using FishNet.Object;
using UnityEngine;

public class NetworkPlayer : NetworkBehaviour
{
    public float RotateSpeed = 150f;
    public float MoveSpeed = 5f;
    private CharacterController _characterController;

    private void Awake()
    {
        _characterController = GetComponent<CharacterController>();
    }

    private void Update()
    {
        if (!base. IsOwner)
            return;

        var horizontal = Input.GetAxisRaw("Horizontal");
        var vertical = Input.GetAxisRaw("Vertical");
        transform.Rotate( eulers:new Vector3(x:0f, y:horizontal * RotateSpeed * Time.deltaTime));
        var offset = new Vector3(0f, Physics.gravity.y, vertical) * (MoveSpeed * Time.deltaTime);
        offset = transform. TransformDirection(offset);

        _characterController.Move(offset);
    }
}
